"""curated_mongos testing package."""


import pathlib

import toml

import curated_mongos


def test_version() -> None:
    """Check that all the version tags are in sync."""

    # Check for pyproject.toml in two places in case of nonlocal install.
    toml_path = pathlib.Path("pyproject.toml")
    if toml_path.exists():
        pyproject_path = toml_path
    else:
        pyproject_path = pathlib.Path(curated_mongos.__file__).parents[2] / "pyproject.toml"
    expected = toml.load(pyproject_path)["tool"]["poetry"]["version"]

    actual = curated_mongos.__version__
    assert actual == expected
