# curated_mongos

![](https://img.shields.io/pypi/v/curated_mongos)
![](https://img.shields.io/pypi/pyversions/curated_mongos.svg)
![](https://github.com/wolfgangwazzlestrauss/curated_mongos/workflows/build/badge.svg)
![](https://img.shields.io/badge/code%20style-black-000000.svg)
![](https://img.shields.io/gitlab/repo-size//curated_mongos)
![](https://img.shields.io/gitlab/license//curated_mongos)

---

**Documentation**: https://.gitlab.io/curated_mongos

**Source Code**: https://gitlab.com/frederick.ollinger/curated_mongos

---

curated_mongos

## Getting Started

### Installation

curated_mongos can be installed for Python 3.6+ with

```bash
pip install --user curated_mongos
```

## Contributing

For guidance on setting up a development environment and making a contribution
to curated_mongos, see the [contributing
guide](CONTRIBUTING.md).

## License


curated_mongos is proprietary software and prohibited from
unauthorized redistribution. See the [license](LICENSE.md) for more
information.
