"""Command line interface for curated_mongos.

See https://docs.python.org/3/using/cmdline.html#cmdoption-m for why module is
named __main__.py.
"""


import typer


app = typer.Typer(
    help="Curated Pixel Data in MongoDB"
)


if __name__ == "__main__":
    app()
